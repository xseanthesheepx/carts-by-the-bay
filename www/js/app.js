//Carts By The Bay v 1.0
//Sean Sullivan
//Last Edit 12/21/16

angular.module('starter', ['ionic', 'ngCordova', 'firebase'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }

    });

})

.config(function($stateProvider, $urlRouterProvider, $compileProvider, $ionicConfigProvider) {

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|tel):/);
    $ionicConfigProvider.views.maxCache(0);

    $stateProvider
        .state('map', {
            url: '/',
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl'
        })


    .state('payment', {
        url: '/',
        templateUrl: 'templates/payment.html',
        contoller: 'PayCtrl'

    })

    .state('landing', {
        url: '/',
        templateUrl: 'templates/landing.html',
        controller: 'LandCtrl'
    });

    $urlRouterProvider.otherwise("/");

})


.controller('MapCtrl', function($scope, $state, $ionicPopup) {
    var options = {
        timeout: 10000,
        enableHighAccuracy: true
    };

// ¯\_(ツ)_/¯   
    
    initMap();

    function initMap() {

        var origin_place_id = null;
        var destination_place_id = null;
        var travel_mode = 'DRIVING';
        var map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {
                lat: 30.5230,
                lng: -87.9033
            },
            zoom: 13,
            disableDefaultUI: true
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(initialLocation);


                var marker = new google.maps.Marker({
                    position: initialLocation,
                    map: map
                });

            });
        }

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        directionsDisplay.setMap(map);

        var origin_input = document.getElementById('origin-input');
        var destination_input = document.getElementById('destination-input');


        map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);


        var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
        origin_autocomplete.bindTo('bounds', map);
        var destination_autocomplete =
            new google.maps.places.Autocomplete(destination_input);
        destination_autocomplete.bindTo('bounds', map);



        function expandViewportToFitPlace(map, place) {
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        }

        origin_autocomplete.addListener('place_changed', function() {
            var place = origin_autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            expandViewportToFitPlace(map, place);

            // If the place has a geometry, store its place ID and route if we have
            // the other place ID
            origin_place_id = place.place_id;
            route(origin_place_id, destination_place_id,
                directionsService, directionsDisplay);

            if (document.getElementById === '') {
                origin_place_id = initialLocation;
            }

        });

        destination_autocomplete.addListener('place_changed', function() {
            var place = destination_autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            expandViewportToFitPlace(map, place);

            // If the place has a geometry, store its place ID and route if we have
            // the other place ID
            destination_place_id = place.place_id;
            route(origin_place_id, destination_place_id,
                directionsService, directionsDisplay);
        });

        function route(origin_place_id, destination_place_id,
            directionsService, directionsDisplay) {
            if (!origin_place_id || !destination_place_id) {
                return;
            }


            directionsService.route({
                origin: {
                    'placeId': origin_place_id
                },
                destination: {
                    'placeId': destination_place_id
                },
                travelMode: travel_mode
            }, function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }

            });
        }


        //Pickup - todo - default origin to current location if value is null

        var btn = document.getElementById('pickUp');
        btn.addEventListener('click', function pickUpRequest() {

            if (document.getElementById('origin-input').value === '') {
                navigator.geolocation.getCurrentPosition(function(position) {
                    initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    document.getElementById('origin-input').value === initialLocation;

                });

            }

            if (origin_place_id == null) {
                $ionicPopup.alert({
                    title: 'Enter Location',
                    template: 'Please Enter A Location'
                });
                return;
            }

            
            var locationInfo = document.getElementById('origin-input').value;
            var destination = document.getElementById('destination-input').value;
  
            
            var startPoint = origin_autocomplete.getPlace();
            var endPoint = destination_autocomplete.getPlace();
            var startLat = startPoint.geometry.location.lat();
            var startLng = startPoint.geometry.location.lng();
            var endLat = endPoint.geometry.location.lat();
            var endLng = endPoint.geometry.location.lng();
            
            var center = map.getCenter();
            var middle = new google.maps.LatLng(30.488002,-87.895075);
            var start = new google.maps.LatLng(startLat, startLng);
            var end = new google.maps.LatLng(endLat, endLng);
            var startDistance = google.maps.geometry.spherical.computeDistanceBetween(start, middle);
            var endDistance = google.maps.geometry.spherical.computeDistanceBetween(end, middle); 
        
            console.log(startDistance, endDistance);

            if ((startDistance > 6500) || (endDistance > 6500)) {
                $ionicPopup.alert({
                    title: 'Out Of Range',
                    template: 'Carts By The Bay Only Services Downtown Fairhope Alabama'
                });
                return;
            } 
            

          // var cost = 1;
           //var routeDistance = google.maps.geometry.spherical.computeDistanceBetween(start, end);
   
     /*  --Save for later--// Determine if cost is $1 or $5            
           if (routeDistance > 5000) {
                cost = (cost + 4);
                localStorage.setItem('cost', cost);
            } */
             

            // Don't allow user to send data without destination
            if (document.getElementById('destination-input').value === '') {

                console.log('No destination set');
                return;
            }


            localStorage.setItem('location', locationInfo);
            localStorage.setItem('destination', destination);
            localStorage.setItem('pickup', origin_place_id);
            localStorage.setItem('dropoff', destination_place_id);
           

            var confirm = $ionicPopup.confirm({
                title: 'Send A Cart',
                template: 'Send A Cart To ' + locationInfo + '?'
            });

            confirm.then(function(res) {
                if (res) {
                    $state.go('payment');
                } else {
                    return;
                }
            });
        });
        

    // disable ionic data tab
    $scope.disableTap = function() {
        container = document.getElementsByClassName('pac-container');

        angular.element(container).attr('data-tap-disabled', 'true');
        // leave input field if google-address-entry is selected
        angular.element(container).on("click", function() {
            document.getElementById('origin-input').blur();
        });
    };
    }

})


.controller('PayCtrl', function($scope, $state, $ionicPopup) {
    var options = {
        timeout: 10000,
        enableHighAccuracy: true
    };

    $scope.closeButton = function() {
        $state.go('map');
    }

    var pickUp = localStorage.getItem('pickup');
    var dropOff = localStorage.getItem('dropoff');
    var locationInfo = localStorage.getItem('location');
    var destination = localStorage.getItem('destination');
    $scope.cost = localStorage.getItem('cost');

    $scope.orderNow = function() {

        // Send data to firebase
        var firstName = document.getElementById('firstName').value;
        var lastName = document.getElementById('lastName').value;
        var email = document.getElementById('email').value;
        var phone = document.getElementById('phone').value;
        var database = new Firebase('https://carts-by-the-bay.firebaseio.com/');
        var route = database.child('Route');
    
        var phoneno = /^\d{10}$/;
       
        
        if (pickUp == 'null') {

            $ionicPopup.alert({
                title: 'Error',
                template: 'There Was A Problem Processing Your Order'
            });
            $state.go('map');
        } else if (firstName == "") {
            $ionicPopup.alert({
                title: 'Error',
                template: 'Please Enter A Name'
            });
        } else if (phone == "") {
            $ionicPopup.alert({
                title: 'Error',
                template: 'Please Enter A Phone Number'
            });
        } else if (phone.length !=10 ) {
                    $ionicPopup.alert({
                title: 'Error',
                template: 'Please Enter A Valid 10-Digit Phone Number (No Spaces or Dashes)'
            });
        }  else if (email == "") {
            $ionicPopup.alert({
                title: 'Error',
                template: 'Please Enter An Email Address'
            });  
        } else {


            var confirm = $ionicPopup.confirm({
                title: 'Confirm Order',
                template: 'Send A Cart To ' + locationInfo + '?'
            });

            confirm.then(function(res) {
                if (res) {
                    route
                        .push({
                            Name: firstName + " " + lastName,
                            Email: email,
                            Phone: phone,
                            PickUp: pickUp,
                            Location: locationInfo,
                            Destination: destination,
                            DropOff: dropOff
                        })

                    route.on("child_added", function(snap) {
                        snap.forEach(function(childSnapshot) {
                            var key = childSnapshot.key();
                            var childData = childSnapshot.val();
                        });
                    });
                    
                   
                    $ionicPopup.alert({
                        title: 'Confirmation',
                        template: 'Your Order Has Been Placed'
                    });
                    $state.go('landing');
                } else {
                    return;
                }
            });


        }

    };


})

.controller('LandCtrl', function($scope, $state, $ionicPopup) {
    var options = {
        timeout: 10000,
        enableHighAccuracy: true
    };
    $scope.closeButton = function() {
        $state.go('map');
    }
});
